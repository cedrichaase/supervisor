// Modules to control application life and create native browser window
const { app, BrowserWindow, dialog, Menu } = require('electron');
const fs = require('fs');
const Papa = require('papaparse');
const ipc = require('electron').ipcMain;


// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
let mainWindow;

function cleanData(data) {
    return data
        .filter(row => (!!row[0] && !!row[1]))
        .map(row => [ Number(row[0]), Number(row[1]) ])
}

function loadCSV(path) {
    const rawData = fs.readFileSync(path).toString();

    return Papa.parse(rawData, {
        header: false,
        delimiter: ' '
    });
}

function openFile(filename) {
    const parsed = loadCSV(filename);
    parsed.data = cleanData(parsed.data);

    mainWindow.webContents.send('file_opened', filename);

    mainWindow.webContents.send('data', parsed.data);
}

function openFolder(foldername) {
    const allFilenames = fs.readdirSync(foldername);

    const dataFilenames = allFilenames.filter(name => name.endsWith('data'));

    const dataContents = dataFilenames.map(filename => {
        const parsed = loadCSV(`${foldername}/${filename}`);
        parsed.data = cleanData(parsed.data);

        return {
            name: filename,
            data: parsed.data
        }
    });

    mainWindow.webContents.send('files_opened', dataContents);
}

function createWindow () {
  // Create the browser window.
  mainWindow = new BrowserWindow({
    width: 1024,
    height: 500,
    webPreferences: {
      nodeIntegration: true
    }
  });

  // and load the index.html of the app.
  mainWindow.loadFile('index.html');

  // Open the DevTools.
  // mainWindow.webContents.openDevTools();

  // Emitted when the window is closed.
  mainWindow.on('closed', function () {
    // Dereference the window object, usually you would store windows
    // in an array if your app supports multi windows, this is the time
    // when you should delete the corresponding element.
    mainWindow = null
  });

  const template = [
      {
        label: 'File',
        submenu: [
            {
                label: 'Open File...',
                click: function(menuItem, browserWindow, event) {
                    const filenames = dialog.showOpenDialog({ properties: ['openFile'] });

                    if (!filenames || filenames.length < 1) {
                        return;
                    }

                    const filename = filenames[0];

                    openFile(filename);
                }
            },
            {
                label: 'Open Directory...',
                click: function(menuItem, browserWindow, event) {
                    const foldernames = dialog.showOpenDialog({ properties: ['openDirectory'] });

                    if (!foldernames || foldernames.length < 1) {
                        return;
                    }

                    openFolder(foldernames[0]);
                }
            },
            {
                label: 'Save Labels...',
                click: function(menuItem, browserWindow, event) {
                    const filename = dialog.showSaveDialog();


                    ipc.once('labels', function(event, labelData) {
                        const delimiter = ' ';

                        const serializedLabels = labelData
                            .map(row => row.join(delimiter))
                            .join('\n');

                        fs.writeFileSync(filename, serializedLabels);
                    });

                    mainWindow.webContents.send('export_labels', filename);
                }
            }
        ]
      }
  ];


  const menu = Menu.buildFromTemplate(template);
  Menu.setApplicationMenu(menu);
}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', createWindow);

// Quit when all windows are closed.
app.on('window-all-closed', function () {
  // On macOS it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  if (process.platform !== 'darwin') app.quit()
});

app.on('activate', function () {
  // On macOS it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (mainWindow === null) createWindow()
});

// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and require them here.
