# supervisor

A time series data labeling application,
based on [Electron](https://electronjs.org/)
and the [dygraphs](http://dygraphs.com/) charting library.

<img src="https://gitlab.com/cedrichaase/supervisor/raw/master/supervisor.png" height=400px/>

## Installation

```sh
$ npm install
```

## Usage

Start the program:

```sh
$ npm start
```

Open a space-separated `CSV`-like file by using the
application menu.
**Open File** opens a single file, **Open Directory**
opens all files in a given directory with a `.data` file extension.

Click and drag on one of the graphs to select and
zoom into a time range.
Pan around by pressing and holding `Alt` while click-dragging
the graph.
Double click the graph to zoom back out.

The last time range that was zoomed in to
is considered to be the currently *selected* time range.

To label a time region, select it in this manner,
then press `Meta+[1..4]` to assign the corresponding label.

If labeling was performed, a message will be shown below the bottom graph.

To save labels to a `.data` file,
select **Save Labels** from the application menu
and select a path to save them to.

## Development

`main.js` contains filesystem and window system related calls,
like user interactions with the application menu and loading/saving
data.

`renderer.js` contains everything related to the main
contents of the application window, i.e. graphs and user interaction with them.
