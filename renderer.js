const electron = require('electron');
const ipc = electron.ipcRenderer;
const Dygraph = require('dygraphs');

// This file is required by the index.html file and will
// be executed in the renderer process for that window.
// All of the Node.js APIs are available in this process.

window.range = [0, 0];
window.labels = [];

function zoomCallback(minDate, maxDate, yRanges) {
    range = [minDate, maxDate];
}

function clearGraphs() {
    const graphsContainer = document.getElementById('graphs');
    graphsContainer.innerHTML = '';
    document.getElementById('graph').innerHTML = '';
    document.getElementById('log').innerHTML = '';
}

ipc.on('file_opened', function(event, arg) {
    document.getElementById('heading').innerText = arg.split('/').slice(-2).join('/');
});

ipc.on('data', function(event, data) {
    clearGraphs();

    window.labels = data.map(el => [el[0], 0]);

    const div = document.getElementById('graphs');
    new Dygraph(div, data, {
        labels: ['time', 'value'],
        width: 900,
        zoomCallback
    });
});

ipc.on('files_opened', function(event, files) {
    clearGraphs();

    window.labels = files[0].data.map(el => [el[0], 0]);

    for (const file of files) {
        // make new graph div
        const graph = document.createElement('div');
        graph.id = file.name;
        graph.classList = 'graph';
        document.getElementById('graphs').appendChild(graph);

        // render graph
        const graphDiv = document.getElementById(graph.id);
        new Dygraph(graphDiv, file.data, {
            labels: ['time', 'value'],
            width: 900,
            zoomCallback: function(minDate, maxDate, yRanges) {
                zoomCallback(minDate, maxDate, yRanges, file.name);
            },
            title: file.name
        });
    }
});

ipc.on('export_labels', function(event, args) {
    ipc.send('labels', window.labels);
});

window.onkeydown = function(e) {
    // Meta + Number n -> Label selected region with n
    if (['1', '2', '3', '4'].includes(e.key) && e.metaKey === true) {
        const label = e.key;

        const minDate = window.range[0];
        const maxDate = window.range[1];
        window.labels = window.labels.map(elem => {
            const time = elem[0];

            // if elem is in selected range
            if (time >= minDate && time <= maxDate) {
                return [time, +label];
            }

            return elem;
        });

        document.getElementById('log').innerHTML += `Marked ${Math.round(range[0])} - ${Math.round(range[1])} as dangerous (label ${label}). <br/>`;
        console.log(window.labels.filter(e => e[1] === 1).length); // num of data points labelled with "1"
    }
};
